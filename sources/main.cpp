#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <getopt.h>

#include <opencv2/opencv.hpp>

#include "multiclass_svm.h"
#include "util.h"

enum class mode
{
    train,
    classify
};

using namespace std;

void Train(
    const string &data_path,
    const string &save_path,
    bool preprocessed,
    double lambda,
    double bias_multiplier,
    double epsilon,
    double retain_variance,
    bool verbose
)
{
    auto data = ml::ReadData(data_path, true, verbose);
    auto x = get<0>(data);
    auto y = get<1>(data);

    if (preprocessed && !x.empty())
    {
        cout << "normalizing input" << endl;
        auto out = ml::Normalize(x);
        x = get<0>(out);
        double mean = get<1>(out);
        double std_dev = get<2>(out);

        cout << "preprocessing input, retain_variance " << retain_variance << endl;
        auto pca = ml::CreatePCA(x, retain_variance);
        x = ml::ProjectPCA(pca, x);
        cout << "dimensionality after projection " << x.at(0).size() << endl;
        if(verbose)
        {
            cout << "first image after projection: " << endl;
            for (const auto& element: x.at(0))
            {
                cout << element << " ";
            }
            cout << endl;
        }

        x = ml::AddQuadraticInteractions(x);
        if(verbose)
        {
            cout << "add quadratic interactions, dimensionality after ";
            cout << x.at(0).size() << endl;
            cout << "saving pca" << endl;
        }
        ml::SavePCA(save_path + ".pca", pca);
        ml::SaveNormalizationParams(save_path + ".norm", mean, std_dev);
    }
    if(verbose)
    {
        cout << "start learning\n" << endl;
        cout << "lambda " << lambda << endl;
        cout << "bias multiplier " << bias_multiplier << endl;
        cout << "epsilon " << epsilon << endl;
    }

    ml::MulticlassSVM svm;
    svm.Train(x, y, lambda, bias_multiplier, epsilon, verbose);

    cout << "learning is finished\n" << endl;
    ml::SaveModel(svm, save_path + ".svm");
}

void Classify(const string &model_path, 
              const string &input_path, 
              const string &output_path, 
              bool preprocessed,
              bool verbose)
{
    auto svm = ml::ReadModel(model_path + ".svm");
    auto data = ml::ReadData(input_path, false, verbose);
    auto x = get<0>(data);

    if (preprocessed && !x.empty())
    {
        if(verbose)
        {
            cout << "preprocessing input" << endl;
        }
        auto out = ml::LoadNormalizationParams(model_path + ".norm");
        double mean = get<0>(out);
        double std_dev = get<1>(out);
        auto pca = ml::LoadPCA(model_path + ".pca");
        x = ml::Normalize(x, mean, std_dev);
        x = ml::ProjectPCA(pca, x);
        if(verbose)
        {
            cout << "dimensionality after projection " << x.at(0).size() << endl;
        }
        x = ml::AddQuadraticInteractions(x);
        if(verbose)
        {
            cout << "add quadratic interactions, dimensionality after ";
            cout << x.at(0).size() << endl;
        }
    }

    auto predictions = svm.Predict(x);
    ml::SavePredictions(get<2>(data), predictions, output_path, verbose);
}

void help()
{
    cout<<"MNIST recognizer"<<endl;
    cout<<"Author: Igor Diakonov (with help of Ruslan Burakov: https://github.com/kuddai/mnist_svm_example)"<<endl;
    cout<<"  --help -h - show this message"<<endl;
    cout<<"  --verbose -V - verbose logging mode"<<endl;
    cout<<"  --mode -m - Mode of the program. It should be either 'train' or 'classify'. Default is 'train'"<<endl;
    cout<<"Depends on mode, program can take different mandatory arguments."<<endl;
    cout<<"Mandatory arguments for 'train' mode:"<<endl;
    cout<<"  --input  -i <path> - Input MNIST description.txt file"<<endl;
    cout<<"  --model  -m <path> - Output model directory"<<endl<<endl;
    cout<<"Optional arguments for 'train' mode:"<<endl;
    cout<<"  --preprocessed -p - Is input data preprocessed. Default is false"<<endl;
    cout<<"  --lambda -l - Default is 0.01"<<endl;
    cout<<"  --bias -b   - Bias multiplier. Default is 1"<<endl;
    cout<<"  --variance -v  - Retain variance. Default is 0.95"<<endl<<endl;
    cout<<"  --epsilon  -e  - Epsilon. Default is 0.02"<<endl<<endl;
    cout<<"Mandatory arguments for 'classify' mode:"<<endl;
    cout<<"  --model  -n <path> - Model path"<<endl;
    cout<<"  --input  -i <path> - Input MNIST dscription.txt file"<<endl;
    cout<<"  --output -o <path> - Output recognition results directory"<<endl<<endl;
    cout<<"Optional arguments for 'classify' mode:"<<endl;
    cout<<"  --preprocessed -p - Is input data preprocessed. Default is false"<<endl;
}

int main(int argc, char** argv)
{
    int c;
    mode m=mode::train;
    string input="";
    string output="";
    string model="";
    bool   preprocessed=false;
    bool   verbose=false;
    double lambda=0.01f;
    double bias=1;
    double variance=0.95f;
    double epsilon=0.02f;
    static struct option long_options[] =
        {
          /* These options set a flag. */
          {"mode",    required_argument, 0, 'm'},
          {"input",   required_argument, 0, 'i'},
          {"model",   required_argument, 0, 'n'},
          {"output",  required_argument, 0, 'o'},
          {"preprocessed",  no_argument, 0, 'p'},
          {"lambda",  required_argument, 0, 'l'},
          {"bias",    required_argument, 0, 'b'},
          {"variance",required_argument, 0, 'v'},
          {"epsilon", required_argument, 0, 'e'},
          {"help",    no_argument,       0, 'h'},
          {"verbose", no_argument,       0, 'V'},
          {0, 0, 0, 0}
        };
    while(1)
    {
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long (argc, argv, "m:i:o:pl:b:r:hV",
                         long_options, &option_index);
        if(c==-1)
            break;
        switch(c)
        {
            case 'm':
                if(string(optarg)=="train")
                {
                    m=mode::train;
                }
                else if(string(optarg)=="classify")
                {
                    m=mode::classify;
                }
                else
                {
                    cerr<<"Undefined mode. Exit"<<endl;
                    return 1;
                }
                break;
            case 'i':
                input=optarg;
                break;
            case 'o':
                output=optarg;
                break;
            case 'n':
                model=optarg;
                break;
            case 'p':
                preprocessed=true;
                break;
            case 'V':
                verbose=true;
                break;
            case 'l':
                lambda=atof(optarg);
                break;
            case 'e':
                epsilon=atof(optarg);
                break;
            case 'b':
                bias=atof(optarg);
                break;
            case 'v':
                variance=atof(optarg);
                break;
            case 'h':
            default:
                help();
                return 0;
        }
    }
    if(input=="")
    {
        cerr<<"Input option must be specified. Exit"<<endl;
        return 1;
    }
    if(!ml::fileExists(input.c_str()))
    {
        cerr<<"Description file does not exist or cannot be read. Exit"<<endl;
        return 1;
    }
    if(model=="")
    {
        cerr<<"Model option must be specified. Exit"<<endl;
        return 1;
    }
    if(m==mode::classify)
    {
        if(!ml::fileExists((model+".svm").c_str())  ||
           !ml::fileExists((model+".norm").c_str()) ||
           !ml::fileExists((model+".pca").c_str()))
        {
            cerr<<"Model file does not exist or cannot be read. Exit"<<endl;
            return 1;
        }
        if(output=="")
        {
            cerr<<"Output option must be specified for classification mode. Exit"<<endl;
            return 1;
        }
    }
    try
    {
        if (m == mode::train)
        {
            Train(input, 
                  model,
                  preprocessed,
                  lambda,
                  bias,
                  epsilon,
                  variance,
                  verbose);
        }
        else if (m == mode::classify)
        {
            Classify(model,
                     input,
                     output,
                     preprocessed,
                     verbose);
        }
    }
    catch (const exception& e)
    {
        cerr << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cerr << "something went wrong during training" << endl;
        return 1;
    }
    return 0;
}

