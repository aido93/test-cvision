#include <vector>
#include <algorithm>
#include <unordered_map>
#include <iostream>
#include <fstream>

#include "multiclass_svm.h"
#include "util.h"

using namespace std;

namespace ml
{
    vector<int> GetUniqueLabels(const vector<int>& y)
    {
        vector<int> labels(y);
        sort(labels.begin(), labels.end());
        auto it = unique(labels.begin(), labels.end());
        labels.resize(distance(labels.begin(), it));
        return labels;
    }

    MulticlassSVM::MulticlassSVM(const Matrix& models, 
                                 const vector<double>& biases,
                                 const vector<int>& labels):
        models_(models),
        biases_(biases),
        labels_(labels)
    {
        vector<int> clone(labels);
        sort(clone.begin(), clone.end());
        auto it = unique(clone.begin(), clone.end());
        if (it != clone.end()) {
            throw runtime_error("labels contain duplicates");
        }
    }

    void MulticlassSVM::Train(const Matrix &x, 
                              const vector<int> &y,
                              double lambda,
                              double bias_multiplier,
                              double epsilon,
                              bool verbose)
    {
        ValidateTrainData(x, y, false);

        // clear data
        models_.clear();
        biases_.clear();
        labels_ = GetUniqueLabels(y);
        cout << "number of unqiue labels " << labels_.size() << endl;

        for (size_t i = 0; i < labels_.size(); ++i) {
            for (size_t j = i + 1; j < labels_.size(); ++j) {
                // info
                if(verbose)
                {
                    cout << "start svm training one vs one for labels "; 
                    cout << labels_[i] << " " << labels_[j] << endl;
                }

                Matrix sub_x;
                vector<int> sub_y;
                for (size_t k = 0; k < x.size(); ++k) {
                    if (y[k] == labels_[i]) {
                        sub_x.push_back(x[k]);
                        sub_y.push_back(-1);
                    }

                    if (y[k] == labels_[j]) {
                        sub_x.push_back(x[k]);
                        sub_y.push_back(1);
                    }
                }

                BinarySVM svm;
                svm.Train(sub_x, sub_y, lambda, bias_multiplier, epsilon);
                models_.push_back(svm.GetModel());
                biases_.push_back(svm.GetBias());
            }
        }
    }

    vector<int> MulticlassSVM::Predict(const Matrix &x)
    {
        if (x.empty())
            return {};

        if (models_.empty())
            throw runtime_error("there are no models");

        // using majority voting 
        size_t idx = 0;
        vector<vector<int>> raw_predictions;
        for (size_t i = 0; i < labels_.size(); ++i)
        {
            for (size_t j = i + 1; j < labels_.size(); ++j)
            {
                BinarySVM svm(models_[idx], biases_[idx]);
                vector<int> sub_predictions = svm.Predict(x);
                vector<int> raw_prediction;
                for (auto prediction : sub_predictions) {
                    raw_prediction.push_back(prediction == -1 ? labels_[i] : labels_[j]);
                }
                raw_predictions.push_back(raw_prediction);
                ++idx;
            }
        }

        vector<int> predictions;
        for (size_t i = 0; i < x.size(); ++i) {
            unordered_map<int, int> dict;
            int commonest;
            int maxcount = 0;
            for (const auto &raw_prediction : raw_predictions) {
                if (++dict[raw_prediction[i]] > maxcount) {
                    commonest = raw_prediction[i];
                    maxcount = dict[raw_prediction[i]];
                }
            }

            predictions.push_back(commonest);
        }

        return predictions;
    }

    void SaveModel(const ml::MulticlassSVM &svm, const string &save_path) {
        ofstream output(save_path);

        output << svm.GetModels().size() << " " << svm.GetModels()[0].size() << endl;
        for (const auto& model : svm.GetModels())
        {
            for (const auto& val : model)
            {
                output << val << " ";
            }
            output << endl;
        }

        output << svm.GetBiases().size() << endl;
        for (const auto& bias : svm.GetBiases())
        {
            output << bias << endl;
        }

        output << svm.GetLabels().size() << endl;
        for (auto label : svm.GetLabels())
        {
            output << label << endl;
        }
    }

    MulticlassSVM ReadModel(const string& model_path)
    {
        string line;
        size_t models_size, nb_dim, nb_biases, nb_labels;
        ifstream model_file(model_path);  

        if (!(model_file >> models_size >> nb_dim))
            throw length_error(
                "incorrect model file " + model_path +
                ", it has wrong number of models"
            );

        cout << "number of binarySVM models " << models_size << endl;
        cout << "number of dimensions in model " << nb_dim << endl;

        vector<vector<double>> models(models_size);
        for (size_t i = 0; i < models_size; ++i)
        {
            vector<double> model(nb_dim);
            double model_value;
            for (size_t j = 0; j < nb_dim; ++j)
            {
                if (!(model_file >> model_value))
                    throw length_error(
                        "incorrect model file " + model_path +
                        ", model " + to_string(i) + " doesn't have enough values"
                    );
                model[j] = model_value;
            }
            models[i] = model;
        }

        if(!(model_file >> nb_biases))
            throw length_error(
                "incorrect model file " + model_path +
                ", model file has incorrect number of biases"
            );

        vector<double> biases(nb_biases);
        cout << "number of biases " << nb_biases << endl;
        for (size_t i = 0; i < nb_biases; ++i)
        {
            double bias_value;
            if (!(model_file >> bias_value))
                throw length_error(
                    "incorrect model file " + model_path +
                    ", it doesn't have enough biases values"
                );
            biases[i] = bias_value;
        }

        if (!(model_file >> nb_labels))
            throw length_error(
                "incorrect model file " + model_path +
                ", it has wrong number of biases"
            );

        cout << "model has " << nb_labels << " labels" << endl;

        vector<int> labels(nb_labels);
        for (size_t i = 0; i < nb_labels; ++i) {
            int label;
            if (!(model_file >> label))
                throw length_error(
                    "incorrect model file " + model_path +
                    ", it has wrong label"
                );
            labels[i] = label;
        }

        cout << "model defined for labels:";
        for (const auto& label : labels) {
            cout << " " << label;
        }
        cout << endl;

        cout << "finish reading model file " << model_path << endl;

        MulticlassSVM svm(models, biases, labels);
        return svm;
    }


} // namespace ml

