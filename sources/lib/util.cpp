#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include <cmath>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>

#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>

#include "multiclass_svm.h"
#include "util.h"

using namespace std;

namespace ml
{
    const size_t REPORT_THRESHOLD = 1000;

    void ValidateBinaryLabels(const vector<int> &y)
    {
        for (size_t i = 0; i < y.size(); ++i)
        {
            if (y[i] != 1 && y[i] != -1)
            {
                throw out_of_range(
                    "wrong label in " + to_string(i) +
                    ": must be 1 or -1, got " + to_string(y[i])
                );
            }
        }
    }

    void ValidateDimensions(size_t expected, size_t got, size_t idx)
    {
        if (expected != got)
            throw out_of_range(
                "dimension mismatch in input " + to_string(idx) +
                ": expected " + to_string(expected) + 
                ", got " + to_string(got)
            );
    }

    void ValidateTrainData(const Matrix &x, 
                           const vector<int> &y,
                           bool has_binary_labels)
    {
        if (x.empty())
            throw invalid_argument("x is empty");

        if (x.size() != y.size())
            throw invalid_argument("x y have different size");         

        if (has_binary_labels)
            ValidateBinaryLabels(y);

        size_t nb_dim = x[0].size();
        for (size_t i = 0; i < x.size(); ++i)
        {
            ValidateDimensions(nb_dim, x[i].size(), i);
        }
    }

    double DotProduct(const vector<double> v1, const vector<double> v2)
    {
        double dot_product = 0;
        for (size_t i = 0; i < v1.size(); ++i)
        {
            dot_product += v1[i] * v2[i];
        }
        return dot_product;
    }

    Data ReadData(const string &data_path, bool load_label, bool verbose)
    {
        ifstream infile(data_path);  
        string image_path, line;
        int label = -1;
        vector<vector<double>> x; 
        vector<int> y;
        vector<string> image_paths;
        size_t counter = 0;

        while (getline(infile, line))
        {
            istringstream iss(line);

            iss >> image_path;
            if (load_label)
                iss >> label;

            if (verbose && counter % REPORT_THRESHOLD == 0)
                cout << "loaded images " << counter << endl;

            cv::Mat mat = cv::imread(image_path, CV_LOAD_IMAGE_GRAYSCALE);
            vector<double> row;
            for (int i = 0; i < mat.rows; ++i) {
                for (int j = 0; j < mat.cols; ++j) {
                    row.push_back((double)mat.at<uchar>(i, j));
                }
            }

            x.push_back(row);
            y.push_back(label);
            image_paths.push_back(image_path);
            counter++;
        }

        if(verbose)
        {
            cout << "upload all images from " << data_path << endl;
            cout << "number of images " << x.size() << endl;
            cout << "dimensionality " << x.at(0).size() << endl;
            cout << "number of labels "  << y.size() << endl;
            cout << endl; 
        }
        return make_tuple(x, y, image_paths);
    }

    void SavePredictions(const vector<string> &images,
                         const vector<int> &predictions, 
                         const string &output_path,
                         bool verbose)
    {
        if (images.size() != predictions.size())
            throw invalid_argument(
                "can't save predictions as number of images doesn't match number of predictions"
            );
        if(verbose)
        {
            cout << "saving predictions to " << output_path << endl;
        }

        ofstream output(output_path);

        for (size_t i = 0; i < predictions.size(); ++i)
        {
            output << images[i] << " " << predictions[i] << endl;
        }
    }

    void SaveNormalizationParams(const string& path, double mean, double std_dev)
    {
        ofstream output(path);
        output << mean << " " << std_dev << endl;
    }

    tuple<double, double> LoadNormalizationParams(const string &path)
    {
        double mean, std_dev;
        ifstream input(path);  
        if (!(input >> mean >> std_dev))
            throw length_error("incorrect normalization param file " + path);
        return make_tuple(mean, std_dev);
    }

    Matrix Normalize(const Matrix& x, double mean, double std_dev)
    {
        Matrix result(x);
        for (size_t i = 0; i < result.size(); ++i)
        {
            for (size_t j = 0; j < result.at(0).size(); ++j)
            {
                result[i][j] = (result[i][j] - mean) / std_dev;
            }
        }
        return result;
    } 

    tuple<Matrix, double, double> Normalize(const Matrix &x)
    {
        double sum = 0;
        double sum_of_squares = 0;
        double nb = 0;

        for (const auto &row : x)
        {
            for (const auto& value : row)
            {
                sum += value;
                // can it overflow ?
                // 255 * 255 * 60 000
                sum_of_squares += value * value;
                ++nb;
            }
        }

        double mean = sum / nb;
        double variance = (sum_of_squares / nb) - mean * mean;
        double std_dev = sqrt(variance);

        Matrix result = Normalize(x, mean, std_dev);

        return make_tuple(result, mean, std_dev);
    } 

    void SavePCA(const string &path, cv::PCA &pca)
    {
    	cv::FileStorage fs(path, cv::FileStorage::WRITE);  
	    pca.write(fs);  
	    fs.release();  
    }

    cv::PCA LoadPCA(const string &path)
    {
	    cv::FileStorage fs2(path, cv::FileStorage::READ);  
    	cv::PCA pca2;  
	    pca2.read(fs2.root());  
    	fs2.release();
	    return pca2;
    }

    cv::Mat MatrixToCVMat(const Matrix &x)
    {
        cv::Mat result(x.size(), x.at(0).size(), CV_64FC1);
        for (int i = 0; i < result.rows; ++i)
        {
            for (int j = 0; j < result.cols; ++j)
            {
                result.at<double>(i, j) = x[i][j];
            }
        }
        return result;
    }

    Matrix CVMatToMatrix(const cv::Mat &mat)
    {
        Matrix x(mat.rows, vector<double>(mat.cols));
        for (int i = 0; i < mat.rows; ++i)
        {
            for (int j = 0; j < mat.cols; ++j)
            {
                x[i][j] = mat.at<double>(i, j);
            }
        }
        return x;
    }

    cv::PCA CreatePCA(const Matrix &x, double retain_variance)
    {
        cv::Mat mat = MatrixToCVMat(x);
        cv::PCA pca(mat, cv::noArray(), cv::PCA::DATA_AS_ROW,retain_variance); 
        return pca;
    }

    Matrix ProjectPCA(const cv::PCA &pca,  const Matrix &x)
    {
        cv::Mat mat = MatrixToCVMat(x);
        auto result = pca.project(mat);
        return CVMatToMatrix(result);
    }

    Matrix AddQuadraticInteractions(const Matrix &x)
    {
        Matrix result(x);
        for (size_t i = 0; i < x.size(); ++i)
        {
            for (size_t j = 0; j < x.at(0).size(); ++j)
            {
                for (size_t k = j + 1; k < x.at(0).size(); ++k)
                {
                    result[i].push_back(x[i][j] * x[i][k]);
                }
            }
        }

        return result;
    }

    bool dirExists(const char *path)
    {
        struct stat info;

        if(stat( path, &info ) != 0)
            return false;
        else if(info.st_mode & S_IFDIR)
            return true;
        else
            return false;
    }

    bool fileExists(const char *path)
    {
        ifstream ifile(path);
        return (bool)ifile;
    }
} // namespace ml

