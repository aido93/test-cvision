#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "vl/svm.h"

#include "binary_svm.h"

using namespace std;

namespace ml {
    int BinarySVM::Train(const Matrix &x, 
                          const vector<int> &y,
                          double lambda,
                          double bias_multiplier, 
                          double epsilon)
    {
        ValidateTrainData(x, y);

        // reset model
        model_.clear();
        bias_ = 0;

        const vl_size nb_data = x.size();
        const vl_size nb_dim = x[0].size();

        vector<double> raw_x(nb_data * nb_dim);
        vector<double> raw_y(nb_data);

        for (vl_size i = 0; i < nb_data; ++i) {
            for (vl_size j = 0; j < nb_dim; ++j) {
                raw_x[i * nb_dim + j] = x[i][j];
            }
            raw_y[i] = y[i];
        }

        auto deleter = [&](VlSvm* ptr) {
            vl_svm_delete(ptr);
        };

        unique_ptr<VlSvm, decltype(deleter)> svm(
            vl_svm_new(VlSvmSolverSgd,
                       raw_x.data(), nb_dim, nb_data,
                       raw_y.data(),
                       lambda),
            deleter
        );

        vl_svm_set_bias_multiplier(svm.get(), bias_multiplier);
        vl_svm_set_epsilon(svm.get(), epsilon);
        vl_svm_train(svm.get());

        bias_ = vl_svm_get_bias(svm.get());
        const double * raw_model = vl_svm_get_model(svm.get());
        for (vl_size i = 0; i < nb_dim; ++i) {
            model_.push_back(raw_model[i]);
        }
        return vl_svm_get_statistics(svm.get())->iteration;
    }

    vector<int> BinarySVM::Predict(const Matrix &x)
    {
        if (x.empty())
            return {};

        size_t nb_dim = x[0].size();
        ValidateDimensions(nb_dim, model_.size());

        vector<int> predictions;
        predictions.reserve(x.size());
        for (const auto& input: x)
        {
            ValidateDimensions(nb_dim, input.size());
            double dot_product = DotProduct(input, model_);
            int prediction = (dot_product + bias_ > 0) ? 1 : -1;
            predictions.push_back(prediction);
        }

        return predictions;
    }
} // namespace ml

