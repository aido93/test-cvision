#pragma once

#include <vector>
#include "binary_svm.h"

namespace ml {

/**
 * Multiclass SVM which utilises "one vs one" scheme
 */
class MulticlassSVM {
    public: 
        MulticlassSVM() {}
        MulticlassSVM(const Matrix &models, 
                      const std::vector<double> &biases,
                      const std::vector<int> &labels);

        void Train(const  Matrix &x, 
                   const  std::vector<int> &y,
                   double lambda,
                   double bias_multiplier,
                   double epsilon,
                   bool verbose); 

        std::vector<int> Predict(const Matrix &x);

        const Matrix& GetModels() const {return models_;}
        const std::vector<double>& GetBiases() const {return biases_;}
        const std::vector<int>& GetLabels() const {return labels_;}

    private:
        Matrix models_;
        std::vector<double> biases_;
        std::vector<int> labels_;
};

void SaveModel(const MulticlassSVM &svm, const std::string &save_path);

MulticlassSVM ReadModel(const std::string &model_path);

} // namespace ml

