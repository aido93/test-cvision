#pragma once

#include <string>
#include <vector>
#include "util.h"

namespace ml {

class BinarySVM {
    public:
        /**
         * \brief default constructor is not deleted because of
         * ease of future support and for train mode
         */
        BinarySVM() {}

        /**
         * \brief Initialization of SVM for predict mode
         * \param [in] model - previously saved model
         * \param [in] bias  - bias
         */
        BinarySVM(const std::vector<double> &model, double bias):
            model_(model),
            bias_(bias)
            {}

        /**
         * \brief Train model on x,y data
         * \return count of svm iterations on success. -1 on error.
         */
        int Train(const std::vector<std::vector<double>> &x, 
                   const std::vector<int> &y,
                   double lambda = 0.01,
                   // http://www.vlfeat.org/api/svm-fundamentals.html
                   double bias_multiplier = 1,
                   double epsilon = 0.02); 

        /**
         * \brief Use current model to recognize new symbols
         * \return Non-empty vector of ints on success. Empty vector if x is empty
         */
        std::vector<int> Predict(const Matrix& x);

        const std::vector<double>& GetModel() const {return model_;}

        double GetBias() const {return bias_;}

    private:
        std::vector<double> model_;
        double bias_;
};

} // namespace ml

