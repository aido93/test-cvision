#!/bin/bash
set -e

main () {
    ARGS=$@
    POSITIONAL=()
    while [[ $# -gt 0 ]]
    do
        #k="$1"
        pair=( $1 )
        key=${pair[0]}
        value=${pair[1]}

        case $key in
            -m|--mode)
            MODE="$value"
            shift # past argument
            ;;
            *)
            POSITIONAL+=("$1")
            shift
            ;;
        esac
    done
    set -- "${POSITIONAL[@]}"
    if [ "$MODE" = 'validate' ]; then
        echo "Validate"
        echo "Check that needed files are created on previous steps"
        while [ ! -f /testing/description.txt ]; do
            sleep 5
        done
        while [ ! -f /testing/predictions.txt ]; do
            sleep 5
        done
        while [ ! -f /testing/CLASSIFIED ]; do
            sleep 5
        done
        echo "Everything seems to be Ok"
        python3 /validate.py --truth /testing/description.txt --predictions /testing/predictions.txt
    elif [ "$MODE" = 'train' ]; then
        rm -f /ann/TRAINED
        echo "Train"
        while [ ! -f /training/description.txt ]; do
            sleep 5
        done
        /mnist_recognize $ARGS
        echo "Trained"
        touch /ann/TRAINED
    elif [ "$MODE" = 'classify' ]; then
        rm -f /testing/CLASSIFIED
        echo "Check that testing images are loaded"
        while [ ! -f /testing/description.txt ]; do
            sleep 5
        done
        echo "Testing images are loaded"
        echo "Check that model is trained"
        while [ ! -f /ann/TRAINED ]; do
            sleep 5
        done
        echo "Model is trained"
        /mnist_recognize $ARGS
        touch /testing/CLASSIFIED
    else
        echo "/mnist_recognize $ARGS"
        /mnist_recognize $ARGS
    fi
}
main "$@"
