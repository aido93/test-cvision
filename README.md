# Тестовый проект в CVision

# Prerequisites

- linux
- docker
- docker-compose

# How to build

```
git clone git@gitlab.com:aido93/test-cvision.git
cd test-cvision
docker-compose build
```

# How to run

```
docker-compose up
```

# FAQ

- I need raw binary file instead of docker image.

Use `docker cp`, Luke!

- I need to run help or any other command of program?

After building the image run this command (for help)

```
docker run mnist:cvision --help
```

- Why do you use docker?

One day I got tired on building, rebuilding, packing and reinstalling OpenCV with various dependencies and flags.
So I decided to use docker to isolate environment of programs. So you can see everything about versions and build-stage in `sources/Dockerfile`.

- Why don't you use Gitlab CI with docker registry?

Gitlab docker registry has limitations on size of the image. Currently image size is 1.5Gb, so it can be done after reducing image size.
Also Community Gitlab CI has limitations on time of building image. If build stage time exceed this limitation, then I need to pay some money.

# TODO

1. Reduce size of opencv image using multistage building.
2. Use CPack to create .deb and .rpm packages
