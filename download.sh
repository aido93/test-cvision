#!/bin/sh

set -e

download()
{
    if [ ! -f /$2/$1 ]; then
        wget http://yann.lecun.com/exdb/mnist/$1.gz -O /$2/$1.gz
        gzip -d /$2/$1.gz
    fi
}

download train-images-idx3-ubyte training
download train-labels-idx1-ubyte training
download t10k-images-idx3-ubyte  testing
download t10k-labels-idx1-ubyte  testing

if [ ! -f /testing/description.txt ]; then
    pip install pypng
    python convert_mnist_to_png.py / /
    #python convert_mnist_to_png.py / / --verbose
fi
